# ELECTRICAL WIRING OF A 2110 SQ FT HOUSE

Wiring and drafting of a 2110 sq ft house using AUTOCAD with detailed power calculation.   

## Viewing The Project

To view the project, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Autodesk AutoCad


### How-to-view

 * Start AutoCad and open any one of the .dwg files to view the project.
 * Navigate through the tabs to view the different components of the project
 
### Special Note
This project was done in 2012. The calculations and regulations followed were solely based upon the most common practices and conventions as taught in my university. You may have to follow a different convention of design based upon your geographical location, hence your local law. 

## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 
